const NUM_OF_MIXINS = 5
let STATE = {//global STATE. Keep track of what user chose
  sel_base_idx: 0 //selected base index
}

$(function (){
  console.log(`bb code loaded`)
  window.e_selected_f = $('div.selected_f')

  /**
   * Compress slots so that empty slot always stay at the rear end.
   * Assume: box_e the box element thas currently is empty
   * From box_e; moving right, keep swapping until the end
   * @param box_e
   */
  window.compress_empty_slot = function (box_e){
    const index = box_e.index()
    let temp
    for (let i = index; i < e_selected_f.length; i++) {
      temp = $(e_selected_f[i]).data('f')
      $(e_selected_f[i]).data('f', $(e_selected_f[i + 1]).data('f'))
      $(e_selected_f[i]).attr('data-f', $(e_selected_f[i + 1]).data('f'))
      $(e_selected_f[i + 1]).data('f', temp)
      $(e_selected_f[i + 1]).attr('data-f', temp)
      window.e_selected_f = $('div.selected_f')
    }
  }

  /**
   *
   * @returns {number} The index of the empty box. If there's no empty box, return -1
   */
  window.empty_box_index = function (){
    let res = -1
    const box_length = e_selected_f.length
    e_selected_f.each(function (i){
      const f_selected = $(this).data('f')
      if (f_selected == undefined || f_selected < 1) {
        res = i
        return false
      }
      if (i == (box_length - 1)) res = box_length //if we reach the last and still not found an empty box; return length. This indicates that box is full
      return true
    })
    return res
  }

  /**
   * Hide all sweetalerts 2 modals
   * @param ele
   * @param options
   * @return {*}
   */
  window.hide_modal = function (ele = false, options = {}){
    if (! ele) return Swal.close()
    Swal.close() //future close particular modal
  }

  window.refresh_status = function (){
    if (is_all_filled()) {
      $('#flavor_picker button').attr('disabled', 1)
      refresh_edpop()
      show_modal('#all_filled_modal')
      show_overlay()
    } else {
      $('#flavor_picker button').removeAttr('disabled')
      hide_modal('#all_filled_modal')
      if ($('#edit_my_box_modal').css('display') !== 'block') hide_overlay()//if there's no other modal, hide overlay
    }
    $('.jd_spot_remain').text(num_empty_slots())
    if (is_all_filled()) {
      console.info(`Pint filled`)
      $('#is_box_filled').text('Pint filled')
      $('button.cont_next_step').show()
    } else {
      console.info(`Pint not filled`)
      $('#is_box_filled').text('')
      // $('button.cont_next_step').hide()

    }

    //populate summary portion
    const sel_size = $($('.jd_base')[0]).text() || 'vanilla'
    const sel_prod = BASES[sel_size]
    $('#prod_name').text(sel_prod.name)
    $('#price').text(`$` + sel_prod.price)
    let all_flavs_sel = Object.assign({}, C) // all flavors selected, e.g. { 1: 4, 2: 1, 3: 0, 5: 7 }
    for (const flav in all_flavs_sel) all_flavs_sel[flav] = 0 //reset to zero
    $('div.selected_f').each((i, selected_f) => {
      let selected_f_val = selected_f.dataset['f']
      if (isNaN(selected_f_val)) return
      selected_f_val = parseInt(selected_f_val)
      if (! all_flavs_sel.hasOwnProperty(selected_f_val)) all_flavs_sel[selected_f_val] = 0 //add key if missing
      if (selected_f_val > 0) {
        all_flavs_sel[selected_f_val]++ //then count
      } else {
        // all_flavs_sel[selected_f_val] = 0
      }
    })
    let flavs_sel_texts = []
    for (const flav in all_flavs_sel) {
      if (! C[flav] || ! (C[flav].name)) continue
      const count = all_flavs_sel[flav]
      if (count == 0) continue
      flavs_sel_texts.push(`${count} of ${C[flav].name}`)
    }
    const flavs_sel_sum_text = flavs_sel_texts.join(', ')
    $('#sum_flavors').text(flavs_sel_sum_text)

    //Update the .flavor picker to show count
    for (const flav in all_flavs_sel) {
      const count = all_flavs_sel[flav]
      const count_e = $($(`.flavor[data-f="${flav}"]`).find('.f_count'))
      count_e.text(count)
      count_e.toggle(count > 0) //only show if more than zero selected
    }

    $('.jd_num_slot_filled').text(e_selected_f.length - num_empty_slots())
  }

  /**
   * determine if all boxes are filled
   */
  window.is_all_filled = function (){
    let res = false
    if (empty_box_index() === e_selected_f.length) res = true
    return res
  }

  /**
   * determine number of empty slots
   */
  window.num_empty_slots = function (){
    let res = 0
    const box_length = e_selected_f.length
    e_selected_f.each(function (i){
      const f_selected = $(this).data('f')
      if (f_selected == undefined || f_selected < 1) {
        res++
      }
    })
    return res
  }

  /**
   * determine last index of selected flavor, if exists
   * -1 if the flavor wasn't picked
   */
  window.last_idx_flavor = function (flavor_to_find){
    let res = -1
    const box_length = e_selected_f.length
    for (let i = box_length - 1; i >= 0; i--) {
      const slot = e_selected_f[i]
      const f_selected = $(slot).data('f')
      if (f_selected == flavor_to_find) return i
    }
    return res
  }

  /**
   * determine if all boxes are empty
   */
  window.is_all_empty = function (){
    return 'WIP'
  }
  /**
   * determine if current device is mobile
   */
  window.is_mobile = function (){
    return window.innerWidth <= 900
  }

  /**
   * Remove the last slot that has this flavor
   * @return boolean Succeed removing?
   */
  window.remove_last_slot_has_f = function (flavor){
    const last_idx_this_flavor = last_idx_flavor(flavor)
    if (last_idx_this_flavor < 0) {
      console.log(`Nothing to remove. Is this flavor selected?`)
      return false
    }
    set_f(-1, last_idx_this_flavor)
    return true
  }

  /**
   * Scroll an element into view
   * @param selector
   */
  window.scroll_into_view = function (selector){
    const e = $(selector)
    if (e.length != 1) return 'Cannot find element'
    e[0].scrollIntoView()
    let top_offset = 140
    if (is_mobile()) top_offset = 90 //future: top_offset is height of menu
    window.scrollBy(0, 0 - top_offset)
  }
  /**
   * Scroll a selected slot into view
   * @param selector
   */
  window.scroll_selected_slot_into_view = function (selector){
    if (! is_mobile()) return
    /*    const e = (selector instanceof jQuery ? selector : $(selector))
        if (e.length != 1) return 'Cannot find element'
        e[0].scrollIntoView()
        let top_offset = 720
        window.scrollBy(0, 0 - top_offset)*/
  }

  /**
   * Set flavor at a box index
   * @param {number} f
   * @param {number||Object} box_index_or_e Box Index, or the Box element itself
   */
  window.set_f = function (f = -1, box_index_or_e = -1){
    if (typeof box_index_or_e === 'number') box_index_or_e = e_selected_f[box_index_or_e]
    if (! (box_index_or_e instanceof jQuery)) box_index_or_e = $(box_index_or_e)
    //from now on box_index_or_e is jq element
    box_index_or_e.data('f', f)
    box_index_or_e.attr('data-f', f)
    // if (f == -1) compress_empty_slot(box_index_or_e)
    scroll_selected_slot_into_view(box_index_or_e)
    $('#edit_size_btn').show()
    $('#edit_your_box_btn').show()
    refresh_status()
  }

  /**
   * Set flavor to the first empty slot
   * @return boolean
   */
  window.set_f_first_slot = function (flavor){
    let res = false
    e_selected_f.each((i, slot_div) => {
      if (! $(slot_div).data('f') || $(slot_div).data('f') < 0) {
        set_f(flavor, i)
        res = true
        return false
      }
    })
    if (! res) console.error(`Cannot find an empty slot to set flavor ${flavor}`)
    return res
  }
  /*
  Set base (base of pint). Set html for .bc_wrapper
   */
  window.set_base = function (new_base = 0){
    STATE.sel_base_idx = new_base
    $('#sizes').hide()
    $('.selected_f_wrapper').html('')
    for (let i = 1; i <= NUM_OF_MIXINS; i++) {
      let div_selected_f = $('<div class="selected_f"></div>')
      div_selected_f.data('f', -1)
      div_selected_f.attr('data-f', -1)
      div_selected_f.prop('data-f', -1)
      div_selected_f.html('<span class="del"></span>')
      $('.selected_f_wrapper').append(div_selected_f)
    }
    window.e_selected_f = $('div.selected_f')
    // $('.bc_wrapper_m').css('width', 58 * new_base) //only need these for build-box
    // $('.bc_wrapper_m').css('max-width', 58 * new_base) //only need these for build-box
    $('.jd_base').val(new_base)
    $('.jd_base').text(new_base.replace('_', ' '))
    $('#box_content').data('size', new_base)
    $('#box_content').attr('size', new_base)
    refresh_status()
    refresh_edpop()
    scroll_into_view('div.step[step="2"]')
    $('#edit_size_btn').show()
    $('#step2_wrapper').show()
    $('#add_to_cart_btn').removeAttr('disabled')
  }

  window.show_all_filled = function (){
    Swal.fire({
      position: 'middle',
      icon: 'success',
      title: 'You have chosen all flavors for this box',
      showConfirmButton: false,
      timer: 2000
    })
  }

  function show_loading(){
    $('#loading').show()
    setTimeout(() => $('#loading').hide(), 9000) //make sure we make it go away after too long
  }

  /**
   * Shows a modal using sweetalert2
   * @param ele String The element selector
   * @param options Object The default options are: {cente: true}
   * @return Swal Object A newly created swal
   */
  window.show_modal = function (ele, options = {}){
    const html = $(ele).html()
    let width = Math.min(37 * 16, window.innerWidth) //37 em
    Swal.fire({
      html,
      showConfirmButton: false,
      width
    })
    $('.modal_close').hide()
    $('.modal_close').show()
    $('.modal_close').css('border', 'none')
  }

  function hide_loading(){
    $('#loading').hide()
  }

  //switch between desktop and mobile
  if (window.innerWidth < 900) {
    $('#box_content').remove()
    window.e_selected_f = $('div.selected_f')
  } else {
    $('#box_content_m').remove()
    window.e_selected_f = $('div.selected_f')
  }


  //populate bases html
  let p_divs = []
  for (const base in BASES) {
    const b = BASES[base]
    let b_div = $(`<div class="size size_btn" size=${base}>
                      <div class="img"><img src="${b.img}" alt="${base}"></div>
                      <div class="title">${b.name}</div>
                      <div class="box_price">$${b.price}</div>
                      <button class="btn" type="button">SELECT</button>
    </div>`)
    $('#sizes').append(b_div)
  }
  //bind select size buttons (set_size)
  $('.size_btn').on('click', (e) => {
    e.preventDefault()
    e.stopPropagation()
    const base_sel = $(e.target).closest('div.size').attr('size')
    set_base(base_sel)
  })
  //populate box size html END

  //populate flavor html
  /*
    <div class="flavor f1" data-f="1" tag="milk">
      <img src="https://cdn.shopify.com/s/files/1/0523/0051/3467/files/f1.webp?v=1659677856" alt="f1">
      <span class="f_name">Milk Choc</span>
      <button type="button" class="add_f btn btn">Add</button>
      <button type="button" class="detail_f btn button reverse">Details</button>
    </div>
   */
  let f_divs = []
  for (const f in C) {
    const flavor = C[f]
    let f_div = $(`<div class="flavor f${f}" data-f="${f}" tag="${flavor.tag}">
                        <img src="${flavor.img}" alt="flavor">
                        <span class="f_count"></span>
                        <span class="f_name">${flavor.name}</span>
                        <button type="button" class="add_f add_detail_btn btn btn">Add</button>
                        <button type="button" class="detail_f add_detail_btn btn reverse">Details</button>
                        <button class="add_f RoundButton RoundButton--small RoundButton--flat" type="button" aria-label="Zoom" data-action="open-product-zoom">
                          <svg class="Icon Icon--plus " role="presentation" viewBox="0 0 16 16">
                            <g stroke="currentColor" fill="none" fill-rule="evenodd" stroke-linecap="square">
                              <path d="M8,1 L8,15"></path>
                              <path d="M1,8 L15,8"></path>
                            </g>
                          </svg>
                        </button>
                      </div>
`)
    $('#flavors').append(f_div)
  }
  //populate flavor html END
  //populate sleeve html
  let s_div_texts = []
  for (const i in S) {
    const sleeve = S[i]
    let s_div = $('<div class="sleeve"></div>')
    let s_div_text = `<div class="sleeve" data-s="${i}" tag="${sleeve.tag}">
                      <img src="${sleeve.img}" alt="sleeve">
                      <div class="center_wrapper">
                        <button class="sleeve_btn btn" type="button">Select</button>
                      </div>
                    </div>`
    $('#sleeves').append(s_div_text)
  }
  //set_sleeve
  $('button.sleeve_btn, div.sleeve').on('click', (e => {
    e.stopPropagation()
    const sleeve_div = $(e.target).closest('div.sleeve')
    if (sleeve_div.length != 1) return
    const sleeve_index = sleeve_div.data('s')
    if (! S.hasOwnProperty(sleeve_index)) return
    const sl_name = S[sleeve_index].name
    $('.jd_sleeve_picked').text(sl_name)
    $('#sizes').hide()
    // $('#sleeve_wrapper').hide()
    $('#change_sleeve_btn').show()
    $('#edit_size_btn').show()
    //assign label image onto the pint
    $('#selected_l').prop('src', S[sleeve_index].img_curved || S[sleeve_index].img)
    // scroll_into_view('#prod_summary')
    // window.scrollBy(0, 60)
  }))
  $('#change_sleeve_btn').on('click', e => {
    e.stopPropagation()
    $('#step3_wrapper').show()
  })
  //bind label tag buttons
  $('#l_tags a').on('click', (e) => {
    e.stopPropagation()
    $('#l_tags a').removeClass('is-active')
    $(e.target).addClass('is-active')
    let tag = $(e.target).text().trim()
    if (tag == 'all') tag = '' //means  not filtering
    const all_label_divs = $('div.sleeve')
    all_label_divs.hide()
    all_label_divs.each(function (i){
      if ($(this).attr('tag').includes(tag)) $(this).show()
    })

  })

  //populate sleeve html END

  //bind add buttons
  $('button.add_f, div.flavor>img, div.flavor>span.f_name, button#dtl_select').on('click', (e) => {
    e.preventDefault()
    e.stopPropagation() //prevent all_filled popup being detected as outside-clicked
    const e_btn_div = $(e.target).closest('div[data-f]')//closest parent that holds the current flavor
    const f_selected = e_btn_div.data('f')
    set_f(f_selected, empty_box_index())
  })

  //bind change size button
  $('#change_box_btn').on('click', (e) => {
    $('#sizes').show('fast')
  })
  $('button#edit_your_box_btn').on('click', (e) => {
    e.stopPropagation()
    $('#step2_wrapper').show()
  })
  //bind goto step 3 button
  $(document).on('click', 'button.cont_step_3', (e) => {
    $('#step2_wrapper').hide()
    hide_modal()
    scroll_into_view('div.step[step="3"]')
  })
  $('button.cont_step_4').on('click', (e) => {
    $('#step3_wrapper').hide()
    scroll_into_view('#prod_summary')
  })
  //bind change size button
  $('#change_box_size_a').on('click', (e) => {
    window.choices_obj.showDropdown()
    $('#sizes').show()
    scroll_into_view('div.step[step="1"]')
  })

  //bind flavor tag buttons
  $('#f_tags a').on('click', (e) => {
    e.stopPropagation()
    $('#f_tags a').removeClass('is-active')
    $(e.target).addClass('is-active')
    let tag = $(e.target).text().trim()
    if (tag == 'all') tag = '' //means  not filtering
    const all_flavor_divs = $('div.flavor')
    all_flavor_divs.hide()
    all_flavor_divs.each(function (i){
      if ($(this).attr('tag').includes(tag)) $(this).show()
    })

  })

  //bind selected_flavor button to remove selected
  $(document).on('click', 'span.del, .selected_f', (e) => {
    e.preventDefault()
    e.stopPropagation()
    const e_div = $(e.target).closest('div')
    set_f(-1, e_div)
  })
  //bind modal Edit Your Selection button
  //Initialize html for edit my box popup
  window.init_edpop = function (){
    $('#edpop_wrapper').html('')
    for (const f in C) {
      const flavor = C[f]
      let edpop_flav =
        `    <div class="row vert_align" data-f="${f}">
        <span class="col-2"><img class="f_img" alt="flavor" src="${flavor.img}"></span>
        <span class="f_name col-5">${flavor.name}</span>
        <span class="col-1"><i class="bi bi-dash-square-fill clickable"></i></span>
        <span class="col-1"> <span class="f_count" data-f="${f}">0</span></span>
        <span class="col-1"><i class="bi bi-plus-square-fill clickable"></i></span>
        <span class="col-2"><i class="edpop_del bi bi-x-circle-fill clickable"></i></span>
      </div>`
      $('#edpop_wrapper').append(edpop_flav)
    }

    $('.bi-dash-square-fill').on('click', function (e){
      e.stopPropagation()
      const cur_flavor = parseInt($(e.target).closest('div.vert_align').data('f'))
      window.remove_last_slot_has_f(cur_flavor)
      refresh_edpop()
    })
    $('.bi-plus-square-fill').on('click', function (e){
      e.stopPropagation()
      const cur_flavor = parseInt($(e.target).closest('div.vert_align').data('f'))
      window.set_f_first_slot(cur_flavor)
      refresh_edpop()
    })
    $('.edpop_del').on('click', function (e){
      e.stopPropagation()
      const cur_flavor = parseInt($(e.target).closest('div.vert_align').data('f'))
      // window.anihilate_f(cur_flavor)
      while (remove_last_slot_has_f(cur_flavor)) {
      }
      refresh_edpop()
    })
  }
  /*
  Refresh the edit popup. Set f_count to each flavor. Enable/Disable Continue button
   */
  window.refresh_edpop = function (){
    let all_flavs_sel = {} // all flavors selected, e.g. { "1": 4, "2": 1 }
    $('div.selected_f').each((i, selected_f) => {
      const selected_f_val = selected_f.dataset['f']
      const selected_f_val_int = parseInt(selected_f_val)
      if (! isNaN(selected_f_val_int) && selected_f_val_int > 0) {
        if (! all_flavs_sel.hasOwnProperty(selected_f_val)) all_flavs_sel[selected_f_val] = 0 //add key if missing
        all_flavs_sel[selected_f_val]++ //then count
      }
    })
    for (const f in C) {
      $(`.f_count[data-f=${f}]`).html(all_flavs_sel[f] || "0")
      if (all_flavs_sel[f] > 0) $(`.vert_align[data-f="${f}"]`).removeClass(`zero_selected`)
      else $(`.vert_align[data-f="${f}"]`).addClass(`zero_selected`)
    }
    if (window.is_all_filled()) {
      $('.cont_step_3').removeAttr('disabled')
      $('.bi-plus-square-fill').addClass('disabled') // No more adding
      $('#all_filled_modal').hide()
    } else {
      // $('#edpop_cont_step_3').attr('disabled', 'disabled')
      $('.bi-plus-square-fill').removeClass('disabled') // Can still add
    }
    $('.jd_num_slot_filled').text(e_selected_f.length - num_empty_slots())
  }

  //Rebuild html for edit my box popup end
  $(document).on('click', '.edit_box_btn', (e) => {
    e.stopPropagation()
    hide_modal()
    $('.modal').hide()
    console.log(`Show edit-inside-popup`)
    show_overlay()
    window.refresh_edpop()
    $('#edit_my_box_modal').show()
  })

  //bind add_to_cart button
  $(document).on('click', 'button#add_to_cart_btn', async (e) => {
    e.preventDefault()
    const sel_size = $('#box_size').text()
    if (! sel_size || sel_size < 0) return false
    const sel_prod = BASES[sel_size]

    let form_data = {
      'items': [{
        'id': sel_prod.variant_id,
        'quantity': 1,
        'properties': {
          'flavors': $('#sum_flavors').text().trim() + ' ' + $('#sum_sleeve').text().trim()
        }
      }]
    };
    let cart_add_res = false
    try {
      cart_add_res = await fetch(window.Shopify.routes.root + 'cart/add.js', {
        method: 'POST', headers: {
          'Content-Type': 'application/json'
        }, body: JSON.stringify(form_data)
      })
    } catch (e) {
      console.error(`Error adding to cart `, e)
      return false
    }
    if (cart_add_res.status == 200) {
      window.location.href = '/cart/'
    }
  })

  $('#dtl_close, .modal_close_btn, .modal_close').on('click', (e) => {
    $('.modal').hide('fast')
    hide_modal()
    hide_overlay()
  })
  $(document).on('click', '.modal_close', (e) => {
    hide_modal()
  })
  //show detail popup
  //popup size is 550 x 200. Make sure popup does not exceed viewport
  //also set data-f for the popup. We need this, so that the Select button works
  $('button.detail_f').on('click', (e) => {
    e.preventDefault()
    const ee = $(e.target), DTL_HEIGHT = 200, DTL_WIDTH = 550
    const dtl_e = $('#detail_wrapper')

    //populate details text
    const current_f = ee.closest('div.flavor').data('f')
    $('#detail_wrapper').data('f', current_f)
    $('#detail_wrapper').attr('data-f', current_f)
    $('#dtl_heading').text(C[current_f].dtl_heading)
    $('#dtl_description').text(C[current_f].description)
    $('#dtl_img').attr('src', C[current_f].img)

    dtl_e.hide()
    hide_overlay()

    const window_e = $(window)
    const rect = e.target.getBoundingClientRect();
    const top_to_set = Math.min(window_e.height() - DTL_HEIGHT - 50, rect.top)//do not exceed window's height
    const left_to_set = Math.min(window_e.width() - DTL_WIDTH - 50, rect.left)//do not exceed window's width
    /*
    position the detail popup to be where the detail button is
    dtl_e.css('top', top_to_set)
    dtl_e.css('left', left_to_set)*/

    dtl_e.show('fast')
    show_overlay()
  })
  $('#dtl_select').on('click', () => {
    $('#detail_wrapper').hide()
    hide_overlay()
  })
  $('button.clear_selections').on('click', () => {
    const len = e_selected_f.length
    for (let i = 0; i < len; i++) {
      set_f(-1, i)
    }
    $('.modal').hide()
    hide_overlay()
  })

  //if click outside modal popup, hide it
  $(document).on('click', (e) => {
    const ee = $(e.target)
    if (ee.hasClass('detail_f')) return //propagate
    const closest_modal = $(e.target).closest('div.modal')
    if (closest_modal.length == 1) return //propagate
    const closest_dtl = $(e.target).closest('#detail_wrapper')
    if (closest_dtl.length < 1) {
      $('.modal').hide()
      hide_overlay()
    }
  })

  hide_loading()
  const choicejs = document.querySelector('.choices');
  window.choices_obj = new Choices(choicejs, {searchEnabled: false, maxItemCount: 1, shouldSort: false, itemSelectText: ''});
  choicejs.addEventListener(
    'change',
    function (event){
      console.log(`choicejs change triggered`)
      const new_size = event.detail.value
      set_base(new_size)
      choices_obj.setChoiceByValue('EDIT SIZE')
    },
    false,
  )
  choicejs.addEventListener(
    'showDropdown',
    function (event){
      $('#edit_size_btn_wrapper').addClass('dropdown_open')
    },
    false,
  )
  choicejs.addEventListener(
    'hideDropdown',
    function (event){
      $('#edit_size_btn_wrapper').removeClass('dropdown_open')
    },
    false,
  )
  $('#edit_size_btn,#edit_size_btn_wrapper').on('click', function (e){
    e.stopPropagation()
    if ($('div.choices.is-open').length == 1) {
      choices_obj.hideDropdown()
    } else {
      choices_obj.showDropdown()
    }
  })


//debugging
//   console.log(`ebi`, empty_box_index())
  window.init_edpop()
//   $('#edit_my_box_modal').show()

// set_f(2, 4)
})

function show_overlay(){
  $('.PageOverlay').addClass('is-visible')
  setTimeout(() => $('.PageOverlay').removeClass('is-visible'), 300000)
}

function hide_overlay(){
  $('.PageOverlay').removeClass('is-visible')
}
